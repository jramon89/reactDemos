const http = require('http');
const fs = require('fs');

var srv = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html' });

    var index = fs.readFileSync('./index.html');

    res.end(index);
    
});

srv.listen(9080,'127.0.0.1',()=>{
	console.log('Server runing')
})