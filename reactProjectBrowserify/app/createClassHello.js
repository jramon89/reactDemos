var React = require('react');
var ReactDOM = require('react-dom');

var MyTag = React.createClass({
	render:function(){
		return (<h1>Hello</h1>);
	}
});

ReactDOM.render(
  <MyTag/>,
  document.getElementById('example')
);