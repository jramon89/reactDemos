var React = require('react');
var ReactDOM = require('react-dom');

var Main = React.createClass({
	render:function(){
		return (
			<section>
				<Aside content={this.props.content}/>
				<Section content={this.props.content}/>
			</section>
		)
	}
});

var Aside = React.createClass({
	render:function(){
		return (
			<aside>
				<h1>Aside {this.props.content}</h1>
			</aside>
		)
	}
})

var Section = React.createClass({
	render:function(){
		return(
			<section>
				<h1>Section {this.props.content}</h1>
			</section>
		)
	}
})

ReactDOM.render(
	<Main content="Value" />,

	document.getElementById('example')
)