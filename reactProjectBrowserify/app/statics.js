var React = require('react');
var ReactDOM = require('react-dom');

var StaticProp = React.createClass({
            statics: {
                componentName: 'Jose R.'
            },
            fn: function() {

            },
            render: function() {
                return ( < div > Static value: {StaticProp.componentName}< /div>);
                }
            })

        ReactDOM.render( <StaticProp/>,
            document.getElementById('example')
        )
