var React = require('react');
var ReactDOM = require('react-dom');

var MyState = React.createClass({
    getInitialState: function() {
        return {
            name: 'Jose Ramon'
        }
    },
    render: function() {
        return ( < h1 > { this.state.name } < /h1>);
        
    }
});

ReactDOM.render( < MyState / > ,
    document.getElementById('example')
);
