var React = require('react');
var ReactDOM = require('react-dom');

var MyProps = React.createClass({
    render: function() {
        return ( < div >
            < div > My prop is {this.props.id} < /div> < /div>
        );
    }
})

ReactDOM.render( 
	
	< MyProps id="José" / > ,
    document.getElementById('example')
);
